import React from 'react';
import '../assetss/css/login.css';
import logo from '../assetss/img/logo_v2.jpeg';
import {Link} from 'react-router-dom';
import {api_register} from '../services/apirest';
import axios from 'axios';

class Register extends React.Component{

    constructor(props){
        super(props);
    }

    state = {
        form:{"email": "","first_name": "", "last_name": "", "password": "", "phone": ""}, error: false, errorMsg: ""
    }

    handlerChange = async (e) =>{
        await this.setState({
            form:{
                //hacer referencia
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
        //console.log(this.state.form);
    }

    handlerRegister = (e)=>{
        e.preventDefault();
        let url = api_register;
        axios.post(url, this.state.form)
        .then(res => {
            if(res.status === 201){
                this.props.history.push("/");
            }
            else{
                this.setState({error:true, errorMsg:res.data.message}); 
            }
        })
        .catch(error =>{
            console.log(error);
            this.setState({
                error: true,
                errorMsg: 'Error to conect api'
            })
        })
    }

    render(){
        return(
            <React.Fragment>
            <div className="wrapper fadeInDown">
                <div id="formContent">
                    <div className="fadeIn first">
                    <br /><br />
                    <img src={logo} width="80px" alt="User Icon" />
                    <br /><br />
                    </div>
                    <form onSubmit={this.handlerRegister}>
                        <input type="text" className="fadeIn second" name="email" placeholder="Email" onChange={this.handlerChange} />
                        <input type="text" className="fadeIn second" name="first_name" placeholder="First Name" onChange={this.handlerChange} />
                        <input type="text" className="fadeIn second" name="last_name" placeholder="Last Name" onChange={this.handlerChange} />
                        <input type="password" className="fadeIn third" name="password" placeholder="Password" onChange={this.handlerChange} />
                        <input type="text" className="fadeIn third" name="phone" placeholder="Phone" onChange={this.handlerChange} />
                        <input type="submit" className="fadeIn fourth" value="Register"/>
                    </form>
                    <Link id = "register" to="/">
                            Login now
                    </Link><br /><br />
                </div>
            </div>
        </React.Fragment>       
        );
    }
}

export default Register