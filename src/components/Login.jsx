import React from 'react';
import '../assetss/css/login.css';
import logo from '../assetss/img/logo_v2.jpeg';
import {Link} from 'react-router-dom';
import {api_login} from '../services/apirest';
import axios from 'axios';
import jwt_decode from "jwt-decode";

class Login extends React.Component{
    
    constructor(props){
        super(props);
    }

    state = {
        form:{"username": "","password": ""}, error: false, errorMsg: ""
    }

    
    componentDidMount(){
        /*const token = JSON.parse(localStorage.getItem('TOKEN'));
        //decodificar token
        const {uid} = jwt_decode(token);
        const url = `http://localhost:3001/api/users/role/${uid}`;

        axios.get(url,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            if(res.data.rol === 'admin'){
                this.props.history.push("/manager");
            }else if(res.data.rol === 'cliente'){
                this.props.history.push("/dashboard");
            }
        })
        .catch(error =>{
            console.log(error.message);
            this.props.history.push("/");
        })*/
    }

    handlerChange = async (e) =>{
        await this.setState({
            form:{
                //hacer referencia
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
        //console.log(this.state.form);
    }

    handlerLogin = (e)=>{
        e.preventDefault();
        let url = api_login;
        axios.post(url, this.state.form)
        .then(res => {
            if(res.statusText === 'OK'){
                localStorage.setItem('TOKEN', JSON.stringify(res.data.token));
                if(localStorage.getItem('TOKEN') != null){
                    this.props.history.push("/twilio2fa");
                }else{
                    this.setState({error:true, errorMsg:'No user found'});   
                    localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'' );
                }
            }
            else{
                this.setState({error:true, errorMsg:res.data.message}); 
                localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
            }
        })
        .catch(error =>{
            console.log(error);
            localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
            this.setState({
                error: true,
                errorMsg: 'Username or Password invalid'
            })
        })
    }

    render(){
        return(
            <React.Fragment>
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <div className="fadeIn first">
                        <br /><br />
                        <img src={logo} width="80px" alt="User Icon" />
                        <br /><br />
                        </div>
                        <form onSubmit={this.handlerLogin}>
                            <input type="text" className="fadeIn second" name="username" placeholder="Email" onChange={this.handlerChange}/>
                            <input type="password" className="fadeIn third" name="password" placeholder="Password" onChange={this.handlerChange}/>
                            <input type="submit" className="fadeIn fourth" value="Log In"/>
                        </form>
                        <Link id = "register" to = "/auth_email">
                                Auth Email
                        </Link><br/>
                        <Link id = "register" to = "/register">
                                Register now
                        </Link><br /><br />
                        {this.state.error === true &&
                            <div className="alert alert-danger" role="alert">
                                {this.state.errorMsg}
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default Login