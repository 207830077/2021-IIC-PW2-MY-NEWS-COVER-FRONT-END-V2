import React from 'react';
import '../assetss/css/manager.css';
import {api_categories} from '../services/apirest';
import axios from 'axios'; 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import {Link} from 'react-router-dom';
import jwt_decode from "jwt-decode";


class Manager extends React.Component{ 

    state = {
        data : [],
        modalAdd: false,
        modalDelete: false,
        form:{"id": "", "name": "", "tipoModal": ""}
    }

    modalAdd = ()=>{
        this.setState({modalAdd: !this.state.modalAdd});
    }

    requestGet = async(e) =>{
        const url = api_categories;
        const token = JSON.parse(localStorage.getItem('TOKEN'));

        await axios.get(url,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            this.setState({data: res.data});
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    requestPost = async (e) =>{
        const token = JSON.parse(localStorage.getItem('TOKEN'));
        const url = api_categories;
        delete this.state.form.id;

        await axios.post(url, this.state.form, { headers: {"Authorization" : `${token}`} })
        .then(res => {
            this.modalAdd();
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    requestPut = async()=>{
        const token = JSON.parse(localStorage.getItem('TOKEN'));
        const url = api_categories;
        await axios.put(url, this.state.form, { headers: {"Authorization" : `${token}`} })
        .then(res =>{
            this.modalAdd();
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    requestDelete = async()=>{
        const token = JSON.parse(localStorage.getItem('TOKEN'));
        const url = api_categories;
        await axios.delete(url+"/"+this.state.form.id, { headers: {"Authorization" : `${token}`} })
        .then(res =>{
            this.setState({modalDelete:false});
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    componentDidMount(){
        const token = JSON.parse(localStorage.getItem('TOKEN'));
                    
        //decodificar token
        const {uid} = jwt_decode(token);
        const url = `http://localhost:3001/api/users/role/${uid}`;

        axios.get(url,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            if(res.data.rol === 'admin'){
                this.requestGet();
            }else{
                this.props.history.push("/dashboard");
            }
        })
        .catch(error =>{
            console.log(error.message);
            this.props.history.push("/");
        })
    }

    handlerChange = async (e) =>{
        e.persist();
        await this.setState({
            form:{
                //hacer referencia
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
    }

    selectedCategory = (category)=>{
        this.setState({
            "tipoModal": "update",
            form: {"id": category._id, "name": category.name}
        })
    }

    render(){
        const {form} = this.state
        return(
            <React.Fragment>
                <main>
                    <div>
                        <nav className="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
                            <a className="navbar-brand" href="/manager">My News Cover Manager</a>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/manager">Category</a>
                                    </li>
                                </ul>
                                <div>
                            <span>
                                <Link className="nav-link container p-3" to="/logout">Logout</Link>
                            </span>
                        </div>
                            </div>
                        </nav>
                    </div>
                    <div className="container p-4">
                        <div id="btn-add">
                            <button className="btn btn-success" onClick={()=>{this.setState({form: null, "tipoModal": "add"}); this.modalAdd()}}>Add Category</button>
                        </div>
                        <div>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.data.map((category, pos) =>{
                                        return(
                                            <tr key={pos}>
                                                <td>{category._id}</td>
                                                <td>{category.name}</td>
                                                <td>
                                                    <button className="btn btn-primary" onClick={()=>{this.selectedCategory(category); this.modalAdd()}}><FontAwesomeIcon icon={faEdit}/></button>{"   "}
                                                    <button className="btn btn-danger" onClick={()=>{this.selectedCategory(category); this.setState({modalDelete:true})}}><FontAwesomeIcon icon={faTrashAlt}/></button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                            <Modal isOpen={this.state.modalAdd}>
                                <ModalHeader>
                                    <a id="exit" href="" onClick={()=>this.modalAdd()}>X</a>
                                </ModalHeader>
                                <ModalBody>
                                <div className="form-group">
                                    <h4>Manager Category</h4>
                                    <label htmlFor="nombre">Name:</label>
                                    <input className="form-control" type="text" name="name" id="name" onChange={this.handlerChange} value={form ? form.name: ''} />
                                </div>
                                </ModalBody>
                                <ModalFooter>
                                    {this.state.tipoModal === "add" ?
                                    <button className="btn btn-success" onClick={()=>this.requestPost()}>Add</button>:
                                    <button className="btn btn-primary" onClick={()=>this.requestPut()}>Update</button> }
                                    <button className="btn btn-danger" onClick={()=>this.modalAdd()}>Cancel</button>
                                </ModalFooter>
                            </Modal>

                            <Modal isOpen={this.state.modalDelete}>
                                <ModalBody>
                                    ¿Are you sure you want to remove the {form&&form.name} category?
                                </ModalBody>
                                <ModalFooter>
                                    <button className="btn btn-danger" onClick={()=>this.requestDelete()}>Yes</button>
                                    <button className="btn btn-primary" onClick={()=>this.setState({modalDelete:false})}>No</button>
                                </ModalFooter>
                            </Modal>
                        </div>
                    </div>
                </main>
            </React.Fragment>
        );
    }
}
export default Manager