import React from 'react';
import axios from 'axios'; 

class VerifyAuthEmail extends React.Component{

    constructor(props){
        super(props);
        console.log(props.match.params);
    }


    componentDidMount(){
        if(localStorage.getItem('TOKEN') != null){
            const token = this.props.match.params.token;
            localStorage.setItem('TOKEN', JSON.stringify(token));

            if(this.props.match.params.rol === 'client'){
                this.props.history.push("/dashboard");
            }else if(this.props.match.params.rol === 'admin'){
                this.props.history.push("/manager");
            }
        }else{
            this.setState({error:true, errorMsg:'No user found'});   
            localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'' );
        }
    }


    render(){
        return(
            <React.Fragment>
            </React.Fragment>        
        );
        
    }
}

export default VerifyAuthEmail