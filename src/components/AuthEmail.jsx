import React from 'react';
import '../assetss/css/login.css';
import logo from '../assetss/img/logo_v2.jpeg';
import {Link} from 'react-router-dom';
import axios from 'axios';

class AuthEmail extends React.Component{
    
    constructor(props){
        super(props);
    }

    state = {
        form:{"email": ""}, error: false, errorMsg: ""
    }

    
    componentDidMount(){

    }

    handlerChange = async (e) =>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
    }

    handlerLoginByEmail = (e)=>{
        e.preventDefault();
        let url = 'http://localhost:3001/api/users/auth_email';
        axios.post(url, this.state.form)
        .then(res => {
            if(res.statusText === 'OK'){
                this.props.history.push("/");
            }
            else{
                this.setState({error:true, errorMsg:res.data.message}); 
                localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
            }
        })
        .catch(error =>{
            console.log(error);
            localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
            this.setState({
                error: true,
                errorMsg: 'Email invalid'
            })
        })
    }

    render(){
        return(
            <React.Fragment>
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <div className="fadeIn first">
                        <br /><br />
                        <img src={logo} width="80px" alt="User Icon" />
                        <br /><br />
                        </div>
                        <form onSubmit={this.handlerLoginByEmail}>
                            <input type="text" className="fadeIn second" name="email" placeholder="Email" onChange={this.handlerChange}/>
                            <input type="submit" className="fadeIn fourth" value="Auth Email"/>
                        </form>
                        <Link id = "register" to = "/">
                                Log In
                        </Link><br/>
                        <Link id = "register" to = "/register">
                                Register now
                        </Link><br /><br />
                        {this.state.error === true &&
                            <div className="alert alert-danger" role="alert">
                                {this.state.errorMsg}
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default AuthEmail