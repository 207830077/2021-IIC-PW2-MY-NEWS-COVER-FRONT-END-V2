import React from 'react';
import '../assetss/css/dashboard.css';
import {api_source, api_categories, api_source_add, api_news_add} from '../services/apirest';
import axios from 'axios'; 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt, faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import jwt_decode from "jwt-decode";

const token = JSON.parse(localStorage.getItem('TOKEN'));
const {uid} = jwt_decode(token);

class Dashboard extends React.Component{
    
    state = {
        data : [],
        categories: [],
        url: [],
        modalAdd: false,
        modalDelete: false,
        form:{"id": "", "url": "", "name": "", "category": "", "user": ""  , "tipoModal": ""}
    }

    modalAdd = ()=>{
        this.setState({modalAdd: !this.state.modalAdd});
    }

    modalForm = ()=>{
        this.setState({form:{"id": "", "url": "", "name": "", "category": "", "user": uid}})
    }

    selectedSource = (source)=>{
        this.setState({
            "tipoModal": "update",
            form: {"id": source._id, "url": source.url, "name": source.name, "category": source.category}
        })
    }

    requestGetCategory = (e) =>{
        const url = api_categories;
        axios.get(url,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            this.setState({categories: res.data});
        })
        .catch(error =>{
            console.log(error.message);
        })   
    }

    requestGet = (e) =>{
        const url = api_source;
        axios.get(url+"/?id="+uid, { headers: {"Authorization" : `${token}`} })
        .then(res => {
            this.setState({data: res.data});
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    requestNews = async (e)=>{
        console.log(this.state.form);
        const url = api_news_add;
        await axios.post(url+"/"+uid, this.state.form)
        .then(res => {
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })

    }

    requestPost = async (e) =>{
        const url = api_source_add;
        delete this.state.form.id;
        await axios.post(url, this.state.form, { headers: {"Authorization" : `${token}`} })
        .then(res => {
            this.modalAdd();
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    requestPut = ()=>{
        const url = api_source_add;
        axios.put(url, this.state.form, { headers: {"Authorization" : `${token}`} })
        .then(res =>{
            this.modalAdd();
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        }) 
    }

    requestDelete = async()=>{
        const url = api_source_add;
        await axios.delete(url+"/"+this.state.form.id, { headers: {"Authorization" : `${token}`} })
        .then(res =>{
            this.setState({modalDelete:false});
            this.requestGet();
        })
        .catch(error =>{
            console.log(error.message);
        })
    }

    componentDidMount(){
        const url = `http://localhost:3001/api/users/role/${uid}`;

        axios.get(url,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            if(res.data.rol === 'admin'){
                this.props.history.push("/manager");
            }else{
                this.requestGet();
                this.requestGetCategory();
            }
        })
        .catch(error =>{
            console.log(error.message);
            this.props.history.push("/");
        })
    }

    handlerChange = async (e) =>{
        e.persist();
        await this.setState({
            form:{
                //hacer referencia
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
    }


    render(){
        const {form} = this.state
        return(
            <React.Fragment>
                <main>
                <div className="container p-4">
                        <div id="btn-add">
                            <button className="btn btn-success" onClick={()=>{this.setState({"tipoModal": "add"}); this.modalAdd(); this.modalForm()}}>Add Source</button>
                        </div>
                        <div>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>URL Link</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.data.map((source, pos) =>{
                                        return(
                                            <tr key={pos}>
                                                <td>{source.url}</td>
                                                <td>{source.name}</td>
                                                <td>{source.category_details[0].name}</td>
                                                <td>
                                                    <button className="btn btn-primary" onClick={()=>{this.selectedSource(source); this.modalAdd()}}><FontAwesomeIcon icon={faEdit}/></button>{"   "}
                                                    <button className="btn btn-danger" onClick={()=>{this.selectedSource(source); this.setState({modalDelete:true})}}><FontAwesomeIcon icon={faTrashAlt}/></button>{"   "}
                                                    <button className="btn btn-secondary" onClick={()=>{this.selectedSource(source); this.requestNews()}}><FontAwesomeIcon icon={faFileAlt}/></button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                            <Modal isOpen={this.state.modalAdd}>
                                <ModalHeader>
                                    <a id="exit" href="" onClick={()=>this.modalAdd()}>X</a>
                                </ModalHeader>
                                <ModalBody>
                                <div className="form-group">
                                    <h4>Manager Source</h4>
                                    <label htmlFor="nombre">URL Link:</label>
                                    <input className="form-control" type="text" name="url" id="url" onChange={this.handlerChange}  value={form ? form.url : ''}/>
                                    <label htmlFor="nombre">Name:</label>
                                    <input className="form-control" type="text" name="name" id="name" onChange={this.handlerChange}  value={form ? form.name : ''}/>
                                    <label htmlFor="nombre">Category:</label>
                                    <select name = "category" className="btn btn-light" onChange={this.handlerChange} value={form.category}>
                                        {this.state.categories.map(category=>(
                                            <option key={category._id} value={category._id}>{category.name}</option>
                                        ))}
                                    </select>
                                </div>
                                </ModalBody>
                                <ModalFooter>
                                {this.state.tipoModal === "add" ?
                                    <button className="btn btn-success" onClick={()=>{this.requestPost()}}>Add</button>:
                                    <button className="btn btn-primary" onClick={()=>this.requestPut()}>Update</button> }
                                    <button className="btn btn-danger" onClick={()=>this.modalAdd()}>Cancel</button>
                                </ModalFooter>
                            </Modal>

                            <Modal isOpen={this.state.modalDelete}>
                                <ModalBody>
                                    ¿Are you sure you want to remove the {form&&form.category.name} source?
                                </ModalBody>
                                <ModalFooter>
                                    <button className="btn btn-danger" onClick={()=>this.requestDelete()}>Yes</button>
                                    <button className="btn btn-primary" onClick={()=>this.setState({modalDelete:false})}>No</button>
                                </ModalFooter>
                            </Modal>
                        </div>
                    </div>
                </main>
            </React.Fragment>        
        );
    }
}

export default Dashboard