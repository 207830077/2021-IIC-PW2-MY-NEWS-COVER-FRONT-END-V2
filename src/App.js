//import logo from './logo.svg';
import React from 'react';
import './assetss/css/App.css';
import 'bootstrap/dist/css/bootstrap.css';

import Navigation from './components/Navigation';
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';
import Manager from './components/Manager';
import Dashboard from './components/Dashboard';
import News from './components/News';
import VerifyAccount from './components/VerifyAccount';
import AuthEmail from './components/AuthEmail';
import VerifyAuthEmail from './components/VerifyAuthEmail';
import Twilio2fa from './components/2faTwilio';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    //similar a una etiqueta div
    <React.Fragment>
      <Router>
        <Switch>
          <Route path="/" exact render = {props =>(<Login{...props}/>)}></Route>
          <Route path="/register" exact render = {props =>(<Register{...props}/>)}></Route>
          <Route path="/manager" exact render = {props =>(<Manager{...props}/>)}></Route>
          <Route path="/auth_email" exact render = {props =>(<AuthEmail{...props}/>)}></Route>
          <Route path="/twilio2fa" exact render = {props =>(<Twilio2fa{...props}/>)}></Route>
          <Route path="/user/verify_account/:user" exact render = {props =>(<VerifyAccount{...props}/>)}></Route>
          <Route path="/verify_auth_email/:rol/:token" exact render = {props =>(<VerifyAuthEmail{...props}/>)}></Route>
          <React.Fragment>
            <Navigation></Navigation>
            <Route path="/dashboard" exact render = {props =>(<Dashboard{...props}/>)}></Route>
            <Route path="/news" exact render = {props =>(<News{...props}/>)}></Route>
            <Route path="/logout" exact render = {props =>(<Logout{...props}/>)}></Route>
          </React.Fragment>
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
