import React from 'react';
import axios from 'axios';
import {api_news_get,api_source_add} from '../services/apirest';
import './styles/card.css';
import jwt_decode from "jwt-decode";

const token = JSON.parse(localStorage.getItem('TOKEN'));
const {uid} = jwt_decode(token);

class News extends React.Component{
    state={
        dataNews : [],
        dataSourceCategory: []
    }
    /**
     * Get news by id
     */
    requestGetNewsById(){
        const url = api_news_get;
        let user = JSON.parse(localStorage.getItem('user'));
        axios.get(url+"/"+uid)
        .then(res => {
            const dataNews = res.data; 
            this.setState({ dataNews });
        })
        .catch(error =>{
            console.log(error.message);
        })   
    }
    /**
     * Get category source
     */
    requestGetCategorySources(){
        const url = api_source_add;
        let user = JSON.parse(localStorage.getItem('user'));
        axios.get(url+"/"+uid,  { headers: {"Authorization" : `${token}`} })
        .then(res => {
            const dataSourceCategory = res.data; 
            this.setState({ dataSourceCategory });
            //console.log(dataSourceCategory);
        })
        .catch(error =>{
            console.log(error.message);
        })   
    }
    /**
     * Get news by category and user
     */
    requestGetNewsByCategory = (category)=>{
        const url = api_news_get;
        let user = JSON.parse(localStorage.getItem('user'));
        axios.get(url+"/"+category._id+"/"+uid)
        .then(res => {
            const dataNews = res.data; 
            this.setState({ dataNews });
            //console.log(dataNews);
        })
        .catch(error =>{
            console.log(error.message);
        })
    }
    /**
     * Get all news
     */
    requestGetNewsByIdAll=(e)=>{
        const url = api_news_get;
        let user = JSON.parse(localStorage.getItem('user'));
        axios.get(url+"/"+uid)
        .then(res => {
            const dataNews = res.data; 
            this.setState({ dataNews });
            //console.log(dataNews);
        })
        .catch(error =>{
            console.log(error.message);
        })   
    }

    async componentDidMount() {
       await this.requestGetNewsById();
       await this.requestGetCategorySources();
    }

    render(){
        return(
            <React.Fragment>
                <div className="search">
                    <form class="form-inline">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
                <div className="container">
                {this.state.dataNews.map(({id, subcategory}) => (
                    <div key={id} className = "">
                        <btn className= "btn btn-outline-info btn-md btns">
                            {subcategory}
                        </btn>
                    </div>
                    ))}
                </div>
                <div>
                    <div className = "center">
                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                            <btn className= "btn btn-outline-dark btn-md btns" onClick={()=>{this.requestGetNewsByIdAll();}}>
                                Todas
                            </btn>
                        </div>
                {this.state.dataSourceCategory.map(({id,category}) => (
                        <div key={id} className="btn-group btn-group-toggle" data-toggle="buttons">
                            <btn className= "btn btn-outline-dark btn-md btns" onClick={()=>{this.requestGetNewsByCategory(category);}}>
                                {category.name}
                            </btn>
                        </div>
                    ))}
                    </div>
                <div className="row left">
                    {this.state.dataNews.map(({ date,short_description,title, image_url, permanlink, id,category}) => (
                        <div key={id} className = "col-md-4">
                            <div className = "row">
                                <div className = "col-md-5">
                                    <p className = "nFecha">{date}</p>
                                </div>
                            </div>
                            <div className = "row cont">
                                <div className = "col-md-6 ex2">
                                    <img src={image_url} alt="" />
                                </div>
                            </div>
                            <div className = "row">
                                <div className = "col-md-8">
                                    <a href={permanlink}><p className = "nTitulo"><strong>{title}</strong></p></a>
                                </div>
                                <div className = "col-md-4">
                                <p className = "nCategoria">{category.name}</p>
                                </div>
                            </div>
                            <div className = "row">
                                <div className = "col-md-10">
                                    <p className =  "nDescripcion">{short_description.substr(1,197)+"..."}</p>
                                </div>
                            </div>
                            <div className = "row">
                                <div className = "col-md-6 colora">
                                    <a href={permanlink}>Ver Noticia</a>
                                </div>
                            </div>
                        </div>
                        ))}
                
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default News