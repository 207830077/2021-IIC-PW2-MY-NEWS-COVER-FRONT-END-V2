import React from 'react';
import axios from 'axios'; 

class VerifyAccount extends React.Component{

    constructor(props){
        super(props);
        //console.log(props.match.params.user);
    }


    componentDidMount(){
        const url = `http://localhost:3001/api/users/verify/${this.props.match.params.user}`;
        axios.get(url)
        .then(res => {
            this.props.history.push("/");
        })
        .catch(error =>{
            this.props.history.push("/");
            console.log(error);
        })
    }


    render(){
        return(
            <React.Fragment>
            </React.Fragment>        
        );
        
    }
}

export default VerifyAccount