export const api_login = "http://localhost:3001/api/users/login";
export const api_register = "http://localhost:3001/api/users/register";
export const api_categories = "http://localhost:3001/api/categories";
export const api_source = "http://localhost:3001/api/sources/user";
export const api_source_add = "http://localhost:3001/api/sources";
export const api_source_url_group = "http://localhost:3001/api/sources/url"; 
export const api_news_add = "http://localhost:3001/api/news";
export const api_news_get = "http://localhost:3001/api/news";