import React from 'react';
import '../assetss/css/login.css';
import logo from '../assetss/img/logo_v2.jpeg';
import {Link} from 'react-router-dom';
import axios from 'axios';
import jwt_decode from "jwt-decode";


class AuthEmail extends React.Component{
    
    constructor(props){
        super(props);
    }

    state = {
        form:{"code": ""}, error: false, errorMsg: ""
    }

    
    componentDidMount(){

    }

    handlerChange = async (e) =>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name] : e.target.value
            }
        });
    }

    handler2faTwilio = (e)=>{
        e.preventDefault();
        const token = JSON.parse(localStorage.getItem('TOKEN'));
        /* decodificar token */
        const {uid} = jwt_decode(token);
        /**Traer numero de telefono */
        axios.get(`http://localhost:3001/api/users/phone/${uid}`)
        .then(resp => {
            const phone = resp.data.phone 
            const url = `http://localhost:3001/api/users/twilio/verify/${phone}`;
            axios.post(url, this.state.form)
            .then(res => {
                if(res.data.data.status === 'approved'){
                    axios.get(`http://localhost:3001/api/users/role/${uid}`, 
                        {headers : {"Authorization" : `${token}`} })
                        .then(response => { 
                            if(response.data.rol === 'admin'){
                                this.props.history.push("/manager");
                            }else if(response.data.rol === 'client'){
                                this.props.history.push("/dashboard");
                            }
                        })
                }
                else{
                    this.setState({error:true, errorMsg: 'Phone or Code invalid'}); 
                    localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
                }
            })
            .catch(error =>{
                console.log(error);
                localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
                this.setState({
                    error: true,
                    errorMsg: 'Code invalid'
                })
            })
        })
        .catch(err =>{
            localStorage.setItem('TOKEN', ''+JSON.parse(localStorage.getItem('TOKEN'))+'');
            this.setState({
                err: true,
                errorMsg: 'Error, problem get a phone user'
            })
        })
    }

    render(){
        return(
            <React.Fragment>
                <div className="wrapper fadeInDown">
                    <div id="formContent">
                        <div className="fadeIn first">
                        <br /><br />
                        <img src={logo} width="80px" alt="User Icon" />
                        <br /><br />
                        </div>
                        <form onSubmit={this.handler2faTwilio}>
                            <input type="text" className="fadeIn second" name="code" placeholder="Code" onChange={this.handlerChange}/>
                            <input type="submit" className="fadeIn fourth" value="Verify 2FA"/>
                        </form>
                        <Link id = "register" to = "/">
                                Log In
                        </Link><br/>
                        {this.state.error === true &&
                            <div className="alert alert-danger" role="alert">
                                {this.state.errorMsg}
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default AuthEmail